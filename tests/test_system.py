import random
import unittest
from typing import List

from creepers.component import Registry, Locatable, Orientation
from creepers.lawn import Lawn
from creepers.system import MovementSystem
from tests.helpers import random_moving, random_locatable, set_dimensions, Snowflake, \
    captured_output


class TestSystem(unittest.TestCase):
    def test_nonInitLawn(self):
        Registry.instance().clear()
        Lawn.instance()

        set_dimensions(None, None)

        with self.assertRaises(TypeError):
            system = MovementSystem.instance()
            system._MovementSystem__check_lawn()

    def test_init_emptyRegistry(self):
        Registry.instance().clear()
        lawn = Lawn.instance(1, 1)

        for _ in range(5000):
            lawn.set_width(random.randrange(1, 100))
            lawn.set_height(random.randrange(1, 100))

            actual = MovementSystem.instance()
            actual.reset()

            self.assertEqual(0, len(actual._MovementSystem__entities))
            self.assertTrue(actual.done())

    def test_init_nonEmptyRegistry(self):
        registry = Registry.instance()
        lawn = Lawn.instance(1, 1)
        actual = MovementSystem.instance()

        for _ in range(5000):
            count = random.randrange(1, 100)
            width = random.randrange(1, 100)
            height = random.randrange(1, 100)
            lawn.set_width(width)
            lawn.set_height(height)
            expected_entities: List[int] = []
            expected_done = True
            dump = {}
            registry.clear()
            for _ in range(count):
                value = Snowflake()
                key = id(value)
                dump[key] = value
                locatable = None
                moving = None
                if random.randrange(0, 100) < 90:
                    locatable = random_locatable(width, height)
                if random.randrange(0, 100) < 90:
                    moving = random_moving()
                registry.add(key, locatable, moving)
                if locatable is not None and moving is not None:
                    expected_entities.append(key)
                    expected_done = False

            actual.reset()

            self.assertSequenceEqual(expected_entities, actual._MovementSystem__entities)
            self.assertEqual(expected_done, actual.done())

    def test_update_emptyRegistry(self):
        Registry.instance().clear()
        lawn = Lawn.instance(1, 1)

        for _ in range(5000):
            lawn.set_width(random.randrange(1, 100))
            lawn.set_height(random.randrange(1, 100))
            system = MovementSystem.instance()
            system.reset()
            with captured_output() as (out, err):
                system.update()

            actual = out.getvalue().strip()

            self.assertEqual(0, len(actual))

    def test_update_nonEmptyRegistry(self):
        registry = Registry.instance()
        lawn = Lawn.instance(1, 1)
        system = MovementSystem.instance()

        for _ in range(5000):
            registry.clear()
            count = random.randrange(1, 100)
            width = random.randrange(1, 100)
            height = random.randrange(1, 100)
            lawn.set_width(width)
            lawn.set_height(height)
            entities: List[int] = []
            locatable_components: List[Locatable] = []
            for _ in range(count):
                value = Snowflake()
                key = id(value)
                locatable = random_locatable(width, height)
                moving = random_moving()
                registry.add(key, locatable, moving)
                entities.append(key)
                locatable_components.append(locatable)
            system.reset()
            idx = random.randint(1, count)
            for _ in range(idx):
                with captured_output() as (out, err):
                    system.update()

            actual_output = Locatable.parse(out.getvalue().strip())
            actual_remaining_entities = system._MovementSystem__entities

            self.assertIsNotNone(actual_output)
            self.assertTrue(isinstance(actual_output, Locatable))
            self.assertEqual(str(locatable_components[idx - 1]), str(actual_output))
            self.assertSequenceEqual(entities[idx:], actual_remaining_entities)

    def test_move_right(self):
        registry = Registry.instance()
        lawn = Lawn.instance(1, 1)
        system = MovementSystem.instance()

        for _ in range(5000):
            width = random.randrange(1, 100)
            height = random.randrange(1, 100)
            lawn.set_width(width)
            lawn.set_height(height)
            registry.clear()
            locatable = random_locatable(width, height)
            moving = random_moving()
            registry.add(id(Snowflake()), locatable, moving)
            expected_x, expected_y, expected_orientation = locatable.x, locatable.y, Orientation.East
            if locatable.orientation is Orientation.East:
                expected_orientation = Orientation.South
            elif locatable.orientation is Orientation.South:
                expected_orientation = Orientation.West
            elif locatable.orientation is Orientation.West:
                expected_orientation = Orientation.North
            elif locatable.orientation is Orientation.North:
                expected_orientation = Orientation.East
            system._MovementSystem__face_right(locatable)

            actual_x = locatable.x
            actual_y = locatable.y
            actual_orientation = locatable.orientation

            self.assertEqual(expected_x, actual_x)
            self.assertEqual(expected_y, actual_y)
            self.assertEqual(expected_orientation, actual_orientation)

    def test_move_left(self):
        registry = Registry.instance()
        lawn = Lawn.instance(1, 1)
        system = MovementSystem.instance()

        for _ in range(5000):
            width = random.randrange(1, 100)
            height = random.randrange(1, 100)
            lawn.set_width(width)
            lawn.set_height(height)
            registry.clear()
            key = id(Snowflake())
            locatable = random_locatable(width, height)
            moving = random_moving()
            registry.add(key, locatable, moving)
            expected_x = locatable.x
            expected_y = locatable.y
            expected_orientation = Orientation.East
            if locatable.orientation is Orientation.East:
                expected_orientation = Orientation.North
            elif locatable.orientation is Orientation.South:
                expected_orientation = Orientation.East
            elif locatable.orientation is Orientation.West:
                expected_orientation = Orientation.South
            elif locatable.orientation is Orientation.North:
                expected_orientation = Orientation.West
            system._MovementSystem__face_left(locatable)

            actual_x = locatable.x
            actual_y = locatable.y
            actual_orientation = locatable.orientation

            self.assertEqual(expected_x, actual_x)
            self.assertEqual(expected_y, actual_y)
            self.assertEqual(expected_orientation, actual_orientation)

    def test_move_forward(self):
        registry = Registry.instance()
        lawn = Lawn.instance(1, 1)
        system = MovementSystem.instance()

        for _ in range(5000):
            registry.clear()
            width = random.randrange(1, 100)
            height = random.randrange(1, 100)
            lawn.set_width(width)
            lawn.set_height(height)
            key = id(Snowflake())
            locatable = random_locatable(width, height)
            moving = random_moving()
            registry.add(key, locatable, moving)
            expected_x = locatable.x
            expected_y = locatable.y
            expected_orientation = locatable.orientation
            if locatable.orientation is Orientation.East:
                expected_x += 1
            elif locatable.orientation is Orientation.South:
                expected_y -= 1
            elif locatable.orientation is Orientation.West:
                expected_x -= 1
            elif locatable.orientation is Orientation.North:
                expected_y += 1
            expected_x, expected_y = lawn.clip_location(expected_x, expected_y)
            system._MovementSystem__move_forward(locatable)

            actual_x = locatable.x
            actual_y = locatable.y
            actual_orientation = locatable.orientation

            self.assertEqual(expected_x, actual_x)
            self.assertEqual(expected_y, actual_y)
            self.assertEqual(expected_orientation, actual_orientation)


if __name__ == '__main__':
    unittest.main()
