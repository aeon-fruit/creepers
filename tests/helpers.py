import random
import sys
from contextlib import contextmanager
from io import StringIO
from typing import Optional, Tuple, List

from creepers.component import Locatable, Orientation, Moving, Move
from creepers.lawn import Lawn


class Snowflake:
    pass


def set_dimensions(width: Optional[int], height: Optional[int]):
    lawn = Lawn.instance()
    lawn.set_width(width)
    lawn.set_height(height)


def random_dimensions() -> Tuple[Optional[int], Optional[int]]:
    width = None
    height = None
    if random.randrange(0, 100) < 90:
        width = random.randrange(-100, 100)
    if random.randrange(0, 100) < 90:
        height = random.randrange(-100, 100)
    return width, height


def set_random_dimensions() -> Tuple[Optional[int], Optional[int]]:
    width, height = random_dimensions()
    set_dimensions(width, height)
    if width is not None and width <= 0:
        width = None
    if height is not None and height <= 0:
        height = None
    return width, height


def generate_dimension(upper_bound: Optional[int]) -> Tuple[Optional[int], Optional[int]]:
    if upper_bound is None:
        return random.randint(0, 100), None
    dim = random.randint(0, upper_bound)
    clipped = dim
    out_of_range = random.randrange(0, 100) % 3
    if out_of_range == 0:
        dim += upper_bound + 1
        clipped = upper_bound
    elif out_of_range == 1:
        dim -= upper_bound
        clipped = 0
    return dim, clipped


def random_locatable(width: int, height: int) -> Locatable:
    orientations = [
        Orientation.East,
        Orientation.South,
        Orientation.West,
        Orientation.North,
    ]
    return Locatable(random.randint(0, width), random.randint(0, height), orientations[random.randrange(0, 4)])


def random_moving() -> Moving:
    moves = [
        Move.Forward,
        Move.Right,
        Move.Left,
    ]

    arg: List[Move] = []
    for _ in range(random.randrange(0, 30)):
        arg.append(moves[random.randrange(0, 3)])
    return Moving(arg)


@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err
