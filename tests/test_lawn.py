import unittest

from creepers.lawn import Lawn
from tests.helpers import set_random_dimensions, generate_dimension


class TestLawn(unittest.TestCase):
    def test_dimensions(self):
        actual = Lawn.instance(1, 1)

        for _ in range(5000):
            expected_width, expected_height = set_random_dimensions()

            self.assertEqual(expected_width, actual.width())
            self.assertEqual(expected_height, actual.height())

    def test_clip_location(self):
        lawn = Lawn.instance(1, 1)

        for _ in range(50000):
            width, height = set_random_dimensions()
            x, expected_x = generate_dimension(width)
            y, expected_y = generate_dimension(height)

            actual_x, actual_y = lawn.clip_location(x, y)

            self.assertEqual(expected_x, actual_x)
            self.assertEqual(expected_y, actual_y)

    def test_bool(self):
        lawn = Lawn.instance(1, 1)

        for _ in range(50000):
            width, height = set_random_dimensions()
            expected_bool = not (width is None or height is None)

            actual_bool = False
            if lawn:
                actual_bool = True

            self.assertEqual(expected_bool, actual_bool)


if __name__ == '__main__':
    unittest.main()
