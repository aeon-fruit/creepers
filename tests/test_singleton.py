import random
import unittest

from creepers.singleton import Singleton


class TestSingleton(unittest.TestCase):
    def test_instance_without_args(self):
        @Singleton
        class Argless:
            def __init__(self):
                pass

        expected = id(Argless.instance())

        actual = id(Argless.instance())

        self.assertEqual(expected, actual)

    def test_instance_with_args(self):
        @Singleton
        class Argful:
            def __init__(self, args):
                self.args = args

        expected_args = (1, 2, 3, 5, 8)
        expected = Argful.instance(expected_args)

        for _ in range(500):
            # the subsequent args are ignored since the instance was already initialized
            actual = Argful.instance(
                (random.randint(1, 13), random.randint(1, 21),
                 random.randint(1, 34), random.randint(1, 55)))

            self.assertEqual(id(expected), id(actual))
            self.assertEqual(expected.args, actual.args)
            self.assertEqual(expected_args, actual.args)

    def test_call(self):
        @Singleton
        class Vanilla:
            def __init__(self):
                pass

        with self.assertRaises(TypeError):
            Vanilla()


if __name__ == '__main__':
    unittest.main()
