import random
import unittest
from typing import List

from creepers.component import Orientation, Locatable, Moving, Move, ComponentRegistry, Registry
from tests.helpers import Snowflake


class TestLocatable(unittest.TestCase):
    orientations = [
        Orientation.East,
        Orientation.South,
        Orientation.West,
        Orientation.North,
    ]

    def test_str(self):
        for _ in range(5000):
            x = random.randrange(0, 100)
            y = random.randrange(0, 100)
            orientation = self.orientations[random.randrange(0, 4)]
            expected = str(x) + ' ' + str(y) + ' ' + str(orientation.value)

            actual = str(Locatable(x, y, orientation))

            self.assertEqual(expected, actual)

    def test_parse(self):
        for _ in range(5000):
            x = random.randrange(0, 100)
            y = random.randrange(0, 100)
            orientation = self.orientations[random.randrange(0, 4)]
            expected = Locatable(x, y, orientation)
            string_value = str(expected)

            actual = Locatable.parse(string_value)

            self.assertEqual(expected.x, actual.x)
            self.assertEqual(expected.y, actual.y)
            self.assertEqual(expected.orientation, actual.orientation)


class TestMoving(unittest.TestCase):
    moves = [
        Move.Forward,
        Move.Right,
        Move.Left,
    ]

    def test_init(self):
        for _ in range(5000):
            count = random.randrange(0, 30)
            moves: List[Move] = []
            for _ in range(count):
                moves.append(self.moves[random.randrange(0, 3)])

            actual = Moving(moves)

            self.assertEqual(moves, actual._Moving__moves)
            self.assertEqual(0, actual._Moving__cursor)

    def test_peek(self):
        for _ in range(5000):
            moves: List[Move] = []
            for _ in range(random.randrange(1, 30)):
                moves.append(self.moves[random.randrange(0, 3)])
            moving = Moving(moves)

            move = moving.peek()

            self.assertEqual(0, moving._Moving__cursor)
            self.assertEqual(move, moves[0])

    def test_next(self):
        for _ in range(5000):
            count = random.randrange(1, 100)
            moves: List[Move] = []
            for _ in range(count):
                moves.append(self.moves[random.randrange(0, 3)])
            moving = Moving(moves)

            steps = random.randint(1, count)
            for _ in range(steps):
                move = moving.next()

            self.assertEqual(steps, moving._Moving__cursor)
            self.assertEqual(move, moves[steps - 1])

    def test_str(self):
        for _ in range(5000):
            count = random.randrange(0, 30)
            moves: List[Move] = []
            expected = ''
            for _ in range(count):
                move = self.moves[random.randrange(0, 3)]
                moves.append(move)
                expected += str(move.value)

            actual = str(Moving(moves))

            self.assertEqual(expected, actual)

    def test_parse(self):
        for _ in range(5000):
            count = random.randrange(0, 30)
            moves: List[Move] = []
            for _ in range(count):
                moves.append(self.moves[random.randrange(0, 3)])
            expected = Moving(moves)
            string_value = str(expected)

            actual = Moving.parse(string_value)

            self.assertSequenceEqual(expected._Moving__moves, actual._Moving__moves)
            self.assertEqual(expected._Moving__cursor, actual._Moving__cursor)


class TestComponentRegistry(unittest.TestCase):
    def test_init(self):
        expected = ComponentRegistry()

        self.assertSequenceEqual(expected._ComponentRegistry__attributions, {})

    def test_clear(self):
        expected = ComponentRegistry()

        for _ in range(5000):
            value = Snowflake()
            expected.add(id(value), value)

        expected.clear()

        self.assertSequenceEqual(expected._ComponentRegistry__attributions, {})

    def test_add_NoneComponent(self):
        expected = ComponentRegistry()
        for _ in range(500):
            expected.add(id(Snowflake()), None)

            self.assertSequenceEqual(expected._ComponentRegistry__attributions, {})

    def test_add_duplicateEntityId(self):
        expected = ComponentRegistry()

        dup = id(Snowflake())
        for _ in range(5000):
            actual = Snowflake()
            expected.add(dup, actual)

            self.assertSequenceEqual(expected._ComponentRegistry__attributions, {dup: actual})

    def test_add_duplicateComponent(self):
        expected = ComponentRegistry()

        actual = {}
        dup = Snowflake()
        for _ in range(5000):
            value = Snowflake()
            actual[id(value)] = dup
            expected.add(id(value), dup)

            self.assertSequenceEqual(expected._ComponentRegistry__attributions, actual)

    def test_add_uniqueEntityId(self):
        expected = ComponentRegistry()

        actual = {}
        for _ in range(5000):
            value = Snowflake()
            actual[id(value)] = value
            expected.add(id(value), value)

            self.assertSequenceEqual(expected._ComponentRegistry__attributions, actual)

    def test_entities(self):
        registry = ComponentRegistry()

        expected = []
        for _ in range(5000):
            value = Snowflake()
            expected.append(id(value))
            registry.add(id(value), value)

            actual = registry.entities()

            self.assertSequenceEqual(expected, actual)

    def test_component_NoneEntityId(self):
        registry = ComponentRegistry()

        actual = registry.component(None)

        self.assertIsNone(actual)

    def test_component_nonExistent(self):
        registry = ComponentRegistry()

        for _ in range(5000):
            count = 500
            missing_idx = random.randrange(0, count)
            dump = {}
            missing_id = None
            for i in range(count):
                value = Snowflake()
                if missing_idx == i:
                    missing_id = id(value)
                    # We have to use the value, otherwise it will be reused on next __init__ call
                    dump[id(value)] = value
                else:
                    registry.add(id(value), value)

            actual = registry.component(missing_id)

            self.assertIsNone(actual)

    def test_component_existent(self):
        registry = ComponentRegistry()

        for _ in range(5000):
            count = 500
            fetched_idx = random.randrange(0, count)
            expected = None
            for i in range(count):
                value = Snowflake()
                if fetched_idx == i:
                    expected = value
                registry.add(id(value), value)

            actual = registry.component(id(expected))

            self.assertEqual(expected, actual)


class TestRegistry(unittest.TestCase):
    def test_init(self):
        expected: Registry = Registry.instance()
        expected.clear()

        self.assertSequenceEqual(expected._Registry__locatable.entities(), [])
        self.assertSequenceEqual(expected._Registry__moving.entities(), [])

    def test_add(self):
        expected: Registry = Registry.instance()
        expected.clear()

        actual_locatable = []
        actual_moving = []
        dump = {}
        for _ in range(5000):
            value = Snowflake()
            key = id(value)
            dump[key] = value
            locatable = None
            moving = None
            if random.randrange(0, 100) < 90:
                locatable = Locatable(0, 0, Orientation.East)
                actual_locatable.append(key)
            if random.randrange(0, 100) < 90:
                moving = Moving([Move.Forward, Move.Right, Move.Left])
                actual_moving.append(key)
            expected.add(key, locatable, moving)

        self.assertSequenceEqual(expected._Registry__locatable.entities(), actual_locatable)
        self.assertSequenceEqual(expected._Registry__moving.entities(), actual_moving)

    def test_locatable_entities(self):
        registry: Registry = Registry.instance()
        registry.clear()

        expected = []
        dump = {}
        for _ in range(5000):
            value = Snowflake()
            key = id(value)
            dump[key] = value
            locatable = None
            if random.randrange(0, 100) < 90:
                locatable = Locatable(0, 0, Orientation.East)
                expected.append(key)
            moving = None
            if random.randrange(0, 100) < 90:
                moving = Moving([Move.Forward, Move.Right, Move.Left])
            registry.add(key, locatable, moving)

        actual = registry.locatable_entities()

        self.assertSequenceEqual(expected, actual)

    def test_moving_entities(self):
        registry: Registry = Registry.instance()
        registry.clear()

        expected = []
        dump = {}
        for _ in range(5000):
            value = Snowflake()
            key = id(value)
            dump[key] = value
            moving = None
            if random.randrange(0, 100) < 90:
                moving = Moving([Move.Forward, Move.Right, Move.Left])
                expected.append(key)
            locatable = None
            if random.randrange(0, 100) < 90:
                locatable = Locatable(0, 0, Orientation.East)
            registry.add(key, locatable, moving)

        actual = registry.moving_entities()

        self.assertSequenceEqual(expected, actual)

    def test_locatable_component(self):
        registry: Registry = Registry.instance()
        registry.clear()

        count = 5000
        expected = {}
        dump = {}
        for _ in range(count):
            value = Snowflake()
            key = id(value)
            dump[key] = value
            locatable = None
            if random.randrange(0, 100) < 90:
                locatable = Locatable(0, 0, Orientation.East)
                expected[key] = locatable
            moving = None
            if random.randrange(0, 100) < 90:
                moving = Moving([Move.Forward, Move.Right, Move.Left])
            registry.add(key, locatable, moving)

        actual = {}
        for entity_id in registry.locatable_entities():
            actual[entity_id] = registry.locatable_component(entity_id)

        self.assertSequenceEqual(expected, actual)

    def test_moving_component(self):
        registry: Registry = Registry.instance()
        registry.clear()

        count = 5000
        expected = {}
        dump = {}
        for _ in range(count):
            value = Snowflake()
            key = id(value)
            dump[key] = value
            moving = None
            if random.randrange(0, 100) < 90:
                moving = Moving([Move.Forward, Move.Right, Move.Left])
                expected[key] = moving
            locatable = None
            if random.randrange(0, 100) < 90:
                locatable = Locatable(0, 0, Orientation.East)
            registry.add(key, locatable, moving)

        actual = {}
        for entity_id in registry.moving_entities():
            actual[entity_id] = registry.moving_component(entity_id)

        self.assertSequenceEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
