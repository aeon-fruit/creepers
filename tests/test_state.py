import random
import unittest
from typing import List

from creepers.component import Registry
from creepers.lawn import Lawn
from creepers.state import parse_lines
from tests.helpers import random_moving, random_locatable, set_random_dimensions


class TestState(unittest.TestCase):
    def test_parse_lines_empty(self):
        Registry.instance().clear()

        actual = parse_lines([])

        self.assertFalse(actual)

    def test_parse_lines_missing_lawn(self):
        for _ in range(500):
            Registry.instance().clear()
            lines = []
            for _ in range(random.randrange(1, 100)):
                locatable = random_locatable(100, 100)
                moving = random_moving()
                lines.append(str(locatable))
                lines.append(str(moving))

            actual = parse_lines(lines)

            self.assertFalse(actual)

    def test_parse_lines_no_mowers(self):
        Lawn.instance(1, 1)
        for _ in range(5000):
            Registry.instance().clear()
            expected_width, expected_height = set_random_dimensions()
            while expected_width is None or expected_height is None:
                expected_width, expected_height = set_random_dimensions()
            lines = [str(expected_width) + ' ' + str(expected_height)]

            actual = parse_lines(lines)
            actual_lawn = Lawn.instance()

            self.assertTrue(actual)
            self.assertEqual(expected_width, actual_lawn.width())
            self.assertEqual(expected_height, actual_lawn.height())

    def test_parse_lines_missing_mowers(self):
        Lawn.instance(1, 1)
        for _ in range(5000):
            Registry.instance().clear()
            lines = []
            expected_width, expected_height = set_random_dimensions()
            while expected_width is None or expected_height is None:
                expected_width, expected_height = set_random_dimensions()
            lines.append(str(expected_width) + ' ' + str(expected_height))
            last_miss = 0
            for _ in range(random.randrange(1, 100)):
                miss = random.randrange(0, 2)
                while miss == last_miss:
                    miss = random.randrange(0, 2)
                if miss == 0:
                    lines.append(str(random_locatable(100, 100)))
                elif miss == 1:
                    lines.append(str(random_moving()))
                last_miss = miss

            actual = parse_lines(lines)

            self.assertFalse(actual)

    def test_parse_lines_nothing_missing(self):
        registry: Registry = Registry.instance()
        registry.clear()
        Lawn.instance(1, 1)
        for _ in range(5000):
            registry.clear()
            lines = []
            expected_width, expected_height = set_random_dimensions()
            while expected_width is None or expected_height is None:
                expected_width, expected_height = set_random_dimensions()
            lines.append(str(expected_width) + ' ' + str(expected_height))
            expected_locatable: List[str] = []
            expected_moving: List[str] = []
            for _ in range(random.randrange(1, 100)):
                locatable = str(random_locatable(100, 100))
                moving = str(random_moving())
                lines.append(str(locatable))
                lines.append(str(moving))
                expected_locatable.append(locatable)
                expected_moving.append(moving)

            actual = parse_lines(lines)
            actual_lawn = Lawn.instance()
            actual_locatable: List[str] = []
            actual_moving: List[str] = []
            for key in registry.locatable_entities():
                actual_locatable.append(str(registry.locatable_component(key)))
            for key in registry.moving_entities():
                actual_moving.append(str(registry.moving_component(key)))

            self.assertTrue(actual)
            self.assertEqual(expected_height, actual_lawn.height())
            self.assertEqual(expected_height, actual_lawn.height())
            self.assertSequenceEqual(expected_locatable, actual_locatable)
            self.assertSequenceEqual(expected_moving, actual_moving)


if __name__ == '__main__':
    unittest.main()
