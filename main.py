import sys

from creepers import simulation

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Error: Missing input_file_name argument')
        print('Usage:', sys.argv[0], '<input_file_name>')
    else:
        if simulation(sys.argv[1]):
            print('Success')
        else:
            print('Failure')
