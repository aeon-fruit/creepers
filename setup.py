from setuptools import setup, find_packages

with open('README.md') as f:
    readme_body = f.read()

with open('LICENSE') as f:
    license_body = f.read()

setup(
    name='sample',
    version='0.9.0',
    description='Entity Component System Python POC',
    long_description=readme_body,
    author='Atef N',
    author_email='aeon.fruit@programmer.net',
    url='https://gitlab.com/aeon-fruit/creepers',
    license=license_body,
    packages=find_packages(exclude='tests')
)
