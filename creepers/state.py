import os
from typing import List

from creepers.component import Locatable, Moving, Registry
from creepers.entity import Mower
from creepers.lawn import Lawn


def parse_lines(lines: List[str]) -> bool:
    """
    Load a simulation state from a list of lines.
    :param lines: list of lines representing the simulation state.
    :return: True if the simulation was loaded, False otherwise.
    """
    if len(lines) % 2 != 1:
        return False
    # parse the lawn
    tokens = lines[0].strip().split(" ")
    try:
        width = int(tokens[0])
        height = int(tokens[1])
        Lawn.instance(width, height)
    except ValueError:
        return False
    # parse the mowers
    registry = Registry.instance()
    for i in range(1, len(lines), 2):
        line = lines[i].strip()
        locatable = Locatable.parse(line)
        if locatable is None:
            return False
        line = lines[i + 1].strip()
        moving = Moving.parse(line)
        if moving is None:
            return False
        registry.add(Mower().id, locatable, moving)
    return True


def load_state(file_name: str) -> bool:
    """
    Load a simulation state from an input file.
    :param file_name: input file name.
    :return: True if the simulation was loaded, False otherwise.
    """
    if not os.path.exists(file_name):
        return False
    with open(file_name, 'r') as file:
        lines = file.readlines()
        return parse_lines(lines)
