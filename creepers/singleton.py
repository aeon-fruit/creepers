from typing import TypeVar, Generic

_T = TypeVar('_T')


class Singleton(Generic[_T]):
    """
    Singleton is an generic implementation of the design pattern.
    """

    def __init__(self, cls: type):
        """
        Initialize the actual type of the Singleton.
        :param cls: the actual type of the Singleton.
        """
        self.__cls = cls
        self.__instance: _T = None

    def instance(self, *args) -> _T:
        """
        Get the only instance of the Singleton class.
        :param args: initialization arguments if applicable.
        :return: instance of the Singleton class.
        """
        if self.__instance is None:
            self.__instance = self.__cls(*args)
        return self.__instance

    def __call__(self):
        """
        Override of the __init__ call to prevent construction of Singleton instances.
        :return: nothing.
        :raise: custom TypeError.
        """
        raise TypeError('Singleton ' + str(self.__cls) + ' should be accessed by calling `instance()`.')

    def __instancecheck__(self, inst):
        """
        Check that an instance is a object of the actual type of the Singleton.
        :param inst: instance.
        :return: True if the instance is a object of the actual type of the Singleton, False otherwise.
        """
        return isinstance(inst, self.__cls)
