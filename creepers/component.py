from enum import Enum
from typing import List, Optional, Dict, TypeVar, Generic

from creepers.singleton import Singleton


class Orientation(Enum):
    """
    Orientation is an enumeration of the direction towards which a mower could be oriented.
    """
    East = "E"
    South = "S"
    West = "W"
    North = "N"


class Move(Enum):
    """
    Move is an enumeration of the move types that a mower could perform.
    """
    Forward = "F"
    Right = "R"
    Left = "L"


class Component(object):
    """
    Component holds an attribute of an Entity object. It is one of the three pillars of ECS pattern.
    """
    pass


class Locatable(Component):
    """
    Locatable is a component that holds a position and an orientation (East, South, North, West) of an Entity object.
    """

    def __init__(self, x: int, y: int, orientation: Orientation):
        """
        Initialize position and orientation.
        :param x: x position.
        :param y: y position.
        :param orientation: orientation.
        """
        self.x = x
        self.y = y
        self.orientation = orientation

    def __str__(self) -> str:
        """
        Get string representation of this Locatable.
        :return: string representation of this Locatable.
        """
        return str(self.x) + " " + str(self.y) + " " + str(self.orientation.value)

    @classmethod
    def parse(cls, string_value: str) -> Optional['Locatable']:
        """
        Parse a Locatable from a string representation.
        :param string_value: string representation of a Locatable, ideally, but could be any random string.
        :return: Locatable if the string value is a valid representation of one, None otherwise.
        """
        tokens = string_value.split(" ")
        if len(tokens) != 3:
            return None
        try:
            x = int(tokens[0])
            y = int(tokens[1])
            orientation = Orientation(tokens[2][0])
            return cls(x, y, orientation)
        except ValueError:
            return None


class Moving(Component):
    """
    Moving is a component that holds a set of moves (Forward, Right, Left) of an Entity object.
    """

    def __init__(self, moves: List[Move]):
        """
        Initialize list of moves.
        :param moves: list of moves.
        """
        self.__cursor = 0
        self.__moves = moves

    def peek(self) -> Optional[Move]:
        """
        Get the next move without consuming it.
        :return: next move if any, None otherwise.
        """
        if self.__cursor >= len(self.__moves):
            return None
        return self.__moves[self.__cursor]

    def next(self) -> Optional[Move]:
        """
        Get and consume the next move.
        :return: next move if any, None otherwise.
        """
        move = self.peek()
        if move is not None:
            self.__cursor += 1
        return move

    def __str__(self) -> str:
        """
        Get string representation of this Moving.
        :return: string representation of this Moving.
        """
        return ''.join([str(move.value) for move in self.__moves])

    @classmethod
    def parse(cls, string_value: str) -> Optional['Moving']:
        """
        Parse a Moving from a string representation.
        :param string_value: string representation of a Moving, ideally, but could be any random string.
        :return: Moving if the string value is a valid representation of one, None otherwise.
        """
        moves: List[Move] = []
        try:
            for c in string_value:
                moves.append(Move(c))
            return cls(moves)
        except ValueError:
            return None


_T = TypeVar('_T')


class ComponentRegistry(Generic[_T]):
    """
    ComponentRegistry is a generic registry of Component objects attributed to and Entity.
    """

    def __init__(self):
        """
        Initialize an empty registry.
        """
        # Py3 dict keeps the adding ordered
        self.__attributions: Dict[int, _T] = {}

    def clear(self):
        """
        Clear the registry.
        :return: nothing.
        """
        self.__attributions.clear()

    def add(self, entity_id: int, component: _T):
        """
        Attribute a Component to an Entity through its Id. If None nothing is done.
        :param entity_id: Id of the Entity.
        :param component: Component to attribute to the Entity, could be None.
        :return: nothing.
        """
        if component is not None:
            self.__attributions[entity_id] = component

    def entities(self) -> List[int]:
        """
        Get the list of all the Id of Entity objects having Component objects attributed to them.
        :return: list of all the Id of Entity objects having Component objects attributed to them if any, [] otherwise.
        """
        return [entity_id for entity_id in self.__attributions]

    def component(self, entity_id: int) -> Optional[_T]:
        """
        Get Component object attributed to an Entity through its Id, if any.
        :param entity_id: Id of the Entity.
        :return: Component attributed to the Entity if any, None otherwise.
        """
        return self.__attributions.get(entity_id, None)


@Singleton
class Registry(object):
    """
    Registry is a Singleton aggregate registry of Locatable and Moving Component attributions.
    """

    def __init__(self):
        """
        Initialize underlying Component registries.
        """
        self.__locatable = ComponentRegistry[Locatable]()
        self.__moving = ComponentRegistry[Moving]()

    def clear(self):
        """
        Clear underlying Component registries.
        :return: nothing.
        """
        self.__locatable.clear()
        self.__moving.clear()

    def add(self, entity_id: int, locatable: Optional[Locatable] = None, moving: Optional[Moving] = None):
        """
        Attribute a Locatable and a Moving Component objects to an Entity through its Id.
        :param entity_id: Id of the Entity.
        :param locatable: Locatable Component to attribute to the Entity, could be None.
        :param moving: Moving Component to attribute to the Entity, could be None.
        :return: nothing.
        """
        self.__locatable.add(entity_id, locatable)
        self.__moving.add(entity_id, moving)

    def locatable_entities(self) -> List[int]:
        """
        Get the list of all the Id of Entity objects having Locatable objects attributed to them.
        :return: list of all the Id of Entity objects having Locatable objects attributed to them if any, [] otherwise.
        """
        return self.__locatable.entities()

    def moving_entities(self) -> List[int]:
        """
        Get the list of all the Id of Entity objects having Moving objects attributed to them.
        :return: list of all the Id of Entity objects having Moving objects attributed to them if any, [] otherwise.
        """
        return self.__moving.entities()

    def locatable_component(self, entity_id: int) -> Optional[Locatable]:
        """
        Get Locatable Component object attributed to an Entity through its Id, if any.
        :param entity_id: Id of the Entity.
        :return: Locatable Component attributed to the Entity if any, None otherwise.
        """
        return self.__locatable.component(entity_id)

    def moving_component(self, entity_id: int) -> Optional[Moving]:
        """
        Get Moving Component object attributed to an Entity through its Id, if any.
        :param entity_id: Id of the Entity.
        :return: Moving Component attributed to the Entity if any, None otherwise.
        """
        return self.__moving.component(entity_id)
