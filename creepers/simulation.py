from creepers.state import load_state
from creepers.system import MovementSystem


def simulation(file_name: str) -> bool:
    """
    Run the simulation specified in the input file and return its success status.
    :param file_name: input file path.
    :return: True if the simulation succeeds, False otherwise.
    """
    print('Load state')
    if not load_state(file_name):
        return False
    print('State loaded')
    system = MovementSystem.instance()
    print('Start simulation')
    while not system.done():
        system.update()
    print('Simulation ended')
    return True
