class Mower:
    """
    Mower is an object with an identifier that could be associated with a set of Components.
    It is one of the three pillars of ECS pattern.
    """

    def __init__(self):
        """
        Initialize the Id.
        """
        self.id = id(self)
