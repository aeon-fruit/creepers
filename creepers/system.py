from collections import deque
from typing import Tuple, Dict

from creepers.component import Registry, Move, Orientation, Locatable
from creepers.lawn import Lawn
from creepers.singleton import Singleton


@Singleton
class MovementSystem(object):
    """
    MovementSystem is a System that iterates over the Entity objects on a lawn and updates their locations.
    """
    __ninety: Dict[Orientation, Tuple[Orientation, Orientation]] = {
        Orientation.East: (Orientation.South, Orientation.North),
        Orientation.South: (Orientation.West, Orientation.East),
        Orientation.West: (Orientation.North, Orientation.South),
        Orientation.North: (Orientation.East, Orientation.West),
    }

    def __init__(self):
        """
        Initialize the lawn and the entities to iterate over.
        """
        self.__check_lawn()
        self.__entities = deque([])
        self.reset()

    def done(self) -> bool:
        """
        Check if there are Entity objects in waiting to move.
        :return: True there are Entity objects in waiting to move, False otherwise.
        """
        self.__check_lawn()
        return len(self.__entities) == 0

    def update(self):
        """
        Move the next Entity object in a single epoch if any, do nothing otherwise.
        :return: nothing.
        """
        if self.done():
            return
        registry = Registry.instance()
        entity_id = self.__entities.popleft()
        location = registry.locatable_component(entity_id)
        moves = registry.moving_component(entity_id)
        while moves.peek() is not None:
            move = moves.next()
            if move is Move.Right:
                self.__face_right(location)
            elif move is Move.Left:
                self.__face_left(location)
            elif move is Move.Forward:
                self.__move_forward(location)
        print(str(location))

    def reset(self):
        """
        Reset the entities to iterate over.
        :return: nothing.
        """
        registry = Registry.instance()
        self.__entities = deque([entity_id for entity_id in registry.locatable_entities()
                                 if registry.moving_component(entity_id) is not None])

    @staticmethod
    def __check_lawn():
        """
        Check that the lawn is initialized.
        :return: nothing.
        :raise: custom TypeError if the lawn is not initialized, nothing otherwise.
        """
        if not Lawn.instance():
            raise TypeError('Lawn should be initialized before use by calling '
                            '`Lawn.instance(width=<width>, height=<height>) '
                            'or by setting its width and/or height`.')

    def __face_right(self, location: Locatable):
        """
        Update a Locatable to face the right.
        :param location: Locatable to update.
        :return: nothing.
        """
        location.orientation = self.__ninety[location.orientation][0]

    def __face_left(self, location: Locatable):
        """
        Update a Locatable to face the left.
        :param location: Locatable to update.
        :return: nothing.
        """
        location.orientation = self.__ninety[location.orientation][1]

    @staticmethod
    def __move_forward(location: Locatable):
        """
        Update a Locatable to move forward within the lawn area.
        :param location: Locatable to update.
        :return: nothing.
        """
        lawn = Lawn.instance()
        orientation = location.orientation
        new_x = location.x
        new_y = location.y
        if orientation is Orientation.East:
            new_x += 1
        elif orientation is Orientation.South:
            new_y -= 1
        elif orientation is Orientation.West:
            new_x -= 1
        elif orientation is Orientation.North:
            new_y += 1
        location.x, location.y = lawn.clip_location(new_x, new_y)
