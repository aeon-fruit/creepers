from typing import Optional, Tuple

from creepers.singleton import Singleton


@Singleton
class Lawn(object):
    """
    Lawn defines the playable area of the simulation via a width and a height.
    """

    def __init__(self, width: Optional[int] = None, height: Optional[int] = None):
        """
        Initialize width and height.
        :param width: width, optional and default to None.
        :param height: height, optional and default to None.
        """
        self.__width = None
        self.__height = None
        self.set_width(width)
        self.set_height(height)

    def width(self) -> Optional[int]:
        """
        Get width.
        :return: width, could be None.
        """
        return self.__width

    def height(self) -> Optional[int]:
        """
        Get height.
        :return: height, could be None.
        """
        return self.__height

    def set_width(self, width: Optional[int]):
        """
        Set width.
        :param width: width, optional and default to None.
        :return: nothing.
        """
        self.__width = None
        if width is not None and width > 0:
            self.__width = width

    def set_height(self, height: Optional[int]):
        """
        Set height.
        :param height: height, optional and default to None.
        :return: nothing.
        """
        self.__height = None
        if height is not None and height > 0:
            self.__height = height

    def clip_location(self, x: int, y: int) -> Tuple[int, int]:
        """
        Generate a clipped location on the lawn from a given position.
        If x or y is outside the lawn dimensions, clip it to the nearest bound [0, max).
        :param x: x position.
        :param y: y position.
        :return: clipped location on the lawn based on the given position.
        """
        new_x = None
        if self.__width is not None:
            new_x = min(self.__width, max(0, x))
        new_y = None
        if self.__height is not None:
            new_y = min(self.__height, max(0, y))
        return new_x, new_y

    def __bool__(self) -> bool:
        """
        Get the initialization state of the lawn. True if both dimensions are not None, False otherwise.
        :return: True if width and height are not None, False otherwise.
        """
        return self.__width is not None and self.__height is not None
