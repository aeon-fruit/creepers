# Creepers

This application is a proof of concept for the [Entity Component System (ECS)](https://en.wikipedia.org/wiki/Entity_component_system) architectural pattern widely used in game development.

## Overview

Creepers is a simulation of a game where mowers take turns to mow a lawn area.
The application showcases the ECS pattern's composition over inheritance principle and features:
- a core `simulation` that loads a new game and runs the game loop;
- a `Singleton` `Lawn` which is the playground of the simulation;
- a basic `Mower` entity defined by a unique id to which `Component` objects could be attributed;
- two concrete `Component` derivatives that match the required attributes of being `Locatable` and `Moving`;
- a generic `ComponentRegistry` which is a collection of components attributed to entities;
- a `Singleton` `Registry` that aggregates two `ComponentRegistry` relevant to `Locatable` and `Moving` components; and
- a `Singleton` `MovementSystem` that operates on the `Registry` to execute all the available moves of every single `Locatable` `Moving` entity.

## Running the application

Creepers is a Python application that expects a single argument which is the path of the input file that contains the initial state of the simulation.

## Todo list

Some mechanisms are missing in this application and are deferred to a subsequent version, namely:
- a log system;
- visuals.